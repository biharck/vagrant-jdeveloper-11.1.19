Download JDeveloper [Generic install and BPEL/BPM plugin] and JDK here


1 - Download RPM JDK from here:
http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
And place it here as jdk-7u79-linux-x64.rpm


2 - Download 64 bit Generic Jdeveloper form here:
http://www.oracle.com/technetwork/developer-tools/jdev/downloads/jdeveloper111190-2538883.html
And place it here as jdevstudio11119install.jar


3 - Download BPM extension 11.1.1.9.* form here:
http://www.oracle.com/ocom/groups/public/@otn/documents/webcontent/156082.xml#oracle.bpm.fusion.studio
And place it here as bpm-jdev-extension.zip


4 - Download BPEL extension 11.1.1.9.* form here:
http://www.oracle.com/ocom/groups/public/@otn/documents/webcontent/156082.xml#oracle.bpm.fusion.studio
And place it here as pcbpel_bundle.zip
