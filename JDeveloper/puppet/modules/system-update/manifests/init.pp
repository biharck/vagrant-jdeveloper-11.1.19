class system-update {

  
  exec { 'apt_get_update':
    command => 'sudo apt-get update -y' ,
    timeout => 0
  }

  exec { 'install_gnome':
    command => 'sudo apt-get install kubuntu-desktop --no-install-recommends -y',
    timeout => 0,
    returns => [0, 100],
    require => Exec["apt_get_update"]
  }

exec{ 'startx':
        cwd => "/software",
        command => "sudo cp common/* .",
        require => Exec["install_gnome"]
    }

  exec{ 'move_dependence':
        cwd => "/software",
        command => "sudo cp common/* .",
        require => Exec["startx"]
    }

}

