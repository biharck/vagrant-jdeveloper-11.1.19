class java( $java_archive = "jdk-7u79-linux-x64", $jdev_archive = "jdevstudio11119install"){

    Exec {
        path => [ "/usr/bin", "/bin", "/usr/sbin"]
    }

    exec{ 'install_dependence':
        cwd => "/software",
        command => "sudo dpkg --configure -a",
        require => Class["system-update"],
        timeout => 0,
        returns => [0, 1]

    }


    exec{ 'install_libio':
        cwd => "/software",
        command => "sudo apt-get install alien libaio1 -y",
        require => Exec["install_dependence"],
        returns => [0, 100],
        timeout => 0

    }

    exec{ 'execute_java_rpm':
        cwd => "/software",
        command => "sudo alien --scripts -d  ${java_archive}.rpm",
        require => Exec['install_libio'],
        timeout => 0
        
    }

    exec{ 'change_permission':
        cwd => "/software",
        command => "sudo chmod 777 jdk_1.7.079-1_amd64.deb",                            
        require => Exec['execute_java_rpm']
    }

    exec{ 'install_java':
        cwd => "/software",
        command => "sudo dpkg --install jdk_1.7.079-1_amd64.deb",
        require => Exec['change_permission'],
        timeout => 0
    }

}