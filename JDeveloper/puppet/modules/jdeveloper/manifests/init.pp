class jdeveloper( $jdev_archive = "jdevstudio11119install"){

    Exec {
        path => [ "/usr/bin", "/bin", "/usr/sbin"]
    }

    exec{ 'install_jdeveloper':
        cwd => "/software",
        command => "sudo java -jar ${jdev_archive}.jar -mode=silent -log=install.log",
        require => Class["java"]
    }

    exec { "change_directory_permissions":
        command => "sudo chmod -R 777 /home/vagrant/Oracle/Middleware/jdeveloper/",
        require => Exec["install_jdeveloper"]
    }

    exec { "install_unzip":
        cwd => "/software",
        command => "sudo apt-get install zip unzip -y",
        returns => [0, 100],
        require => Exec["change_directory_permissions"]
    }

    exec { "install_bpel_plugin":
        cwd => "/software",
        command => "sudo unzip -o pcbpel_bundle.zip -d /home/vagrant/Oracle/Middleware/jdeveloper/",
        require => Exec["install_unzip"]
    }

    exec { "install_bpm_plugin":
        cwd => "/software",
        command => "sudo unzip -o bpm-jdev-extension.zip -d /home/vagrant/Oracle/Middleware/jdeveloper/",
        require => Exec["install_bpel_plugin"]
    }

    exec{ 'move_image':
        cwd => "/software",
        command => "sudo cp /software/jdevLogo_48.png /home/vagrant/Oracle/Middleware/jdeveloper/",
        require => Exec['install_bpm_plugin']
    }

    exec{ 'mkdir':
        cwd => "/software",
        command => "sudo mkdir /home/vagrant/Desktop",
        returns => [0, 1],
        require => Exec['move_image']
    }    

    exec{ 'move_shortcut':
        cwd => "/software",
        command => "sudo cp /software/Jdeveloper.desktop /usr/share/applications",
        require => Exec['mkdir']
    }

    exec{ 'change_permission_shortcut':
        cwd => "/usr/share/applications",
        command => "sudo chmod 775 Jdeveloper.desktop",
        require => Exec['move_shortcut']
    }

exec { "shutdown":
        cwd => "/software",
        command => "sudo shutdown -r now",
        timeout => 0,
        require => Exec["change_permission_shortcut"]
    }

}